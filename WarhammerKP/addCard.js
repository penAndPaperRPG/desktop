require('firebase/auth')

var firebase = require("firebase/app");

var firebaseConfig = {
  apiKey: "AIzaSyB5rekUAmGSlShTH1DWFCNaFAL_LRqYzmY",
  authDomain: "warhammer-kp.firebaseapp.com",
  databaseURL: "https://warhammer-kp.firebaseio.com",
  projectId: "warhammer-kp",
  storageBucket: "warhammer-kp.appspot.com",
  messagingSenderId: "773354415332",
  appId: "1:773354415332:web:4bd7470f1532af70"
};

firebase.initializeApp(firebaseConfig);

var admin = require("firebase-admin");

var serviceAccount = require("./warhammer-kp-firebase-adminsdk-sm594-d504c58b61.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://warhammer-kp.firebaseio.com"
});

const db = admin.firestore();

var postCard = document.getElementById('postCard');

var user;
//dodaj item array
function checkButtonId(buttonName, itemID, itemNumber, tabitemsId, addItemInput){
  let itemInputNumber = document.createElement('h4')
  let itemInput = document.createElement('input')
  itemID = buttonName + itemNumber;
  itemInput.id = itemID;
  itemInput.className = "form-control";
  tabitemsId.push(itemInput.id);
  itemInputNumber.textContent = "nr. " + itemNumber;
  addItemInput.appendChild(itemInputNumber)
  addItemInput.appendChild(itemInput);
};

var addItemWealth = document.getElementById('addItemWealth');
var itemWealthID;
var itemsNumberWealth = 1;
var tabIdItemsWealth = [];
const addItemInputWealth = document.querySelector('#addItemInputWealth');

addItemWealth.addEventListener('click', function() {
  checkButtonId('wealth', itemWealthID, itemsNumberWealth++, tabIdItemsWealth, addItemInputWealth);
});

var addItemKnowledge = document.getElementById('addItemKnowledge');
var itemKnowlegeID;
var itemsNumberKnowlege= 1;
var tabIdItemsKnowlege = [];
const addItemInputKnowlege = document.querySelector('#addItemInputKnowlege');

addItemKnowledge.addEventListener('click', function() {
  checkButtonId('knowledge', itemKnowlegeID, itemsNumberKnowlege++, tabIdItemsKnowlege, addItemInputKnowlege);
});

var addItemLanguages = document.getElementById('addItemLanguages');
var itemLanguagesID;
var itemsNumberLanguages= 1;
var tabIdItemsLanguages = [];
const addItemInputLanguages = document.querySelector('#addItemInputLanguages');

addItemLanguages.addEventListener('click', function() {
  checkButtonId('languages', itemLanguagesID, itemsNumberLanguages++, tabIdItemsLanguages, addItemInputLanguages);
});

var addItemAbilities = document.getElementById('addItemAbilities');
var itemAbilitiesID;
var itemsNumberAbilities= 1;
var tabIdItemsAbilities = [];
const addItemInputAbilities = document.querySelector('#addItemInputAbilities');

addItemAbilities.addEventListener('click', function() {
  checkButtonId('abilities', itemAbilitiesID, itemsNumberAbilities++, tabIdItemsAbilities, addItemInputAbilities);
});

var addItemDamage = document.getElementById('addItemDamage');
var itemDamageID;
var itemsNumberDamage= 1;
var tabIdItemsDamage = [];
const addItemInputDamage = document.querySelector('#addItemInputDamage');

addItemDamage.addEventListener('click', function() {
  checkButtonId('damage', itemDamageID, itemsNumberDamage++, tabIdItemsDamage, addItemInputDamage);
});

//usun item array
function checkDelteIdButton(itemNumber, tabitemsId, addItemInput){
  if (itemNumber > 1) {
    //console.log(itemNumber);
    addItemInput.removeChild(addItemInput.lastChild); //ten usuwa inputa
    addItemInput.removeChild(addItemInput.lastChild); //a ten h4
    tabitemsId.splice(itemNumber - 2, 1);
    //itemNumber--;
  } else {
    Swal.fire({
      title: 'Nie mozesz usunąć \nczegoś czego nie ma!',
      text: '',
      imageUrl: './images/fota4.jpg',
      imageWidth: 400,
      imageHeight: 200,
      imageAlt: 'Custom image',
      animation: false
    })
  }
};


var deleteItemWealth = document.getElementById('deleteItemWealth');

deleteItemWealth.addEventListener('click', function () {
  checkDelteIdButton(itemsNumberWealth, tabIdItemsWealth, addItemInputWealth);
  itemsNumberWealth==1 ? itemsNumberWealth : itemsNumberWealth--;
});

var deleteItemKnowledge = document.getElementById('deleteItemKnowledge');

deleteItemKnowledge.addEventListener('click', function () {
  checkDelteIdButton(itemsNumberKnowlege, tabIdItemsKnowlege, addItemInputKnowlege);
  itemsNumberKnowlege==1 ? itemsNumberKnowlege : itemsNumberKnowlege--;
});

var deleteItemLanguages = document.getElementById('deleteItemLanguages');

deleteItemLanguages.addEventListener('click', function () {
  checkDelteIdButton(itemsNumberLanguages, tabIdItemsLanguages, addItemInputLanguages);
  itemsNumberLanguages==1 ? itemsNumberLanguages : itemsNumberLanguages--;
});

var deleteItemAbilities = document.getElementById('deleteItemAbilities');

deleteItemAbilities.addEventListener('click', function () {
  checkDelteIdButton(itemsNumberAbilities, tabIdItemsAbilities, addItemInputAbilities);
  itemsNumberAbilities==1 ? itemsNumberAbilities : itemsNumberAbilities--;
});

var deleteItemDamage = document.getElementById('deleteItemDamage');

deleteItemDamage.addEventListener('click', function () {
  checkDelteIdButton(itemsNumberDamage, tabIdItemsDamage, addItemInputDamage);
  itemsNumberDamage==1 ? itemsNumberDamage : itemsNumberDamage--;
});

//Statystyki cz 1
const StatCol0 = document.querySelector('#StatCol0');

 var meleeStats = [];
 var meleeName = 'melee';
 var meleeNamePL = 'WW'
 var rangeStats = [];
 var rangeName = 'range';
 var rangeNamePL = 'US'
 var strengthStats = [];
 var strengthName = 'strength';
 var strengthNamePL = 'K'
 var toughnessStats = [];
 var toughnessName = 'toughness';
 var toughnessNamePL = 'Odp'
 var agilityStats = [];
 var agilityName = 'agility';
 var agilityNamePL = 'Zr'
 var intelligenceStats = [];
 var intelligenceName = 'intelligence';
 var intelligenceNamePL = 'Int'
 var will_powerStats = [];
 var will_powerName = 'will_power';
 var will_powerNamePL = 'Sw'
 var fellowshipStats = [];
 var fellowshipName = 'fellowship';
 var fellowshipNamePL = 'Ogd'

 arrayStatsFunction(meleeStats, meleeName, meleeNamePL);
 arrayStatsFunction(rangeStats, rangeName, rangeNamePL);
 arrayStatsFunction(strengthStats, strengthName, strengthNamePL);
 arrayStatsFunction(toughnessStats, toughnessName, toughnessNamePL);
 arrayStatsFunction(agilityStats, agilityName, agilityNamePL);
 arrayStatsFunction(intelligenceStats, intelligenceName, intelligenceNamePL);
 arrayStatsFunction(will_powerStats, will_powerName, will_powerNamePL);
 arrayStatsFunction(fellowshipStats, fellowshipName, fellowshipNamePL);

 function arrayStatsFunction(stats, statName, statNamePL){

    var statH4 = document.createElement('h4');
    var stat0 = document.createElement('input');
    var stat1 = document.createElement('input');
    var stat2 = document.createElement('input');

    stat0.type="number"
    stat1.type="number"
    stat2.type="number"

    stats[0] = statName + '0';
    stats[1] = statName + '1';
    stats[2] = statName + '2';

    stat0.id = stats[0];
    stat1.id = stats[1];
    stat2.id = stats[2];

    stat0.className = "form-control";
    stat1.className = "form-control";
    stat2.className = "form-control";

    statH4.textContent = statNamePL;

    StatCol0.appendChild(statH4);
    StatCol0.appendChild(stat0);
    StatCol0.appendChild(stat1);
    StatCol0.appendChild(stat2);
}

//Statystyki cz 2
const StatCol1 = document.querySelector('#StatCol1');

 var attacksStats = [];
 var attacksName = 'attacks';
 var attacksNamePL = 'A'
 var woundsStats = [];
 var woundsName = 'wounds';
 var woundsNamePL = 'Żyw'
//  var strength_bonusStats = [];
//  var strength_bonusName = 'strength_bonus';
//  var strength_bonusNamePL = 'S'
//  var toughness_bonusStats = [];
//  var toughness_bonusName = 'toughness_bonus';
//  var toughness_bonusNamePL = 'Wt'
 var movementStats = [];
 var movementName = 'movement';
 var movementNamePL = 'Sz'
 var magicStats = [];
 var magicName = 'magic';
 var magicNamePL = 'Mag'
 var instanityStats = [];
 var instanityName = 'insanity';
 var instanityNamePL = 'PO'
 var fortuneStats = [];
 var fortuneName = 'fortune';
 var fortuneNamePL = 'PP'

 arrayStatsFunction2(attacksStats, attacksName, attacksNamePL);
 arrayStatsFunction2(woundsStats, woundsName, woundsNamePL);
 arrayStatsFunction2(movementStats, movementName, movementNamePL);
 arrayStatsFunction2(magicStats, magicName, magicNamePL);
 arrayStatsFunction2(instanityStats, instanityName, instanityNamePL);
 arrayStatsFunction2(fortuneStats, fortuneName, fortuneNamePL);

 function arrayStatsFunction2(stats, statName, statNamePL){

    var statH4 = document.createElement('h4');
    var stat0 = document.createElement('input');
    var stat1 = document.createElement('input');
    var stat2 = document.createElement('input');

    stat0.type="number"
    stat1.type="number"
    stat2.type="number"

    stats[0] = statName + '0';
    stats[1] = statName + '1';
    stats[2] = statName + '2';

    stat0.id = stats[0];
    stat1.id = stats[1];
    stat2.id = stats[2];

    stat0.className = "form-control";
    stat1.className = "form-control";
    stat2.className = "form-control";

    statH4.textContent = statNamePL;

    StatCol1.appendChild(statH4);
    StatCol1.appendChild(stat0);
    StatCol1.appendChild(stat1);
    StatCol1.appendChild(stat2);
}

//weapons
const weapons = document.querySelector('#weapons');

 var weapon1Stats = [];
 var weapon1Name = 'weapon1';
 var weapon1NamePL = 'Weapon nr. 1'
 var weapon2Stats = [];
 var weapon2Name = 'weapon2';
 var weapon2NamePL = 'Weapon nr. 2'
 var weapon3Stats = [];
 var weapon3Name = 'weapon3';
 var weapon3NamePL = 'Weapon nr. 3'
 var weapon4Stats = [];
 var weapon4Name = 'weapon4';
 var weapon4NamePL = 'Weapon nr. 4'
 var weapon5Stats = [];
 var weapon5Name = 'weapon5';
 var weapon5NamePL = 'Weapon nr. 5'

 arrayWeaponFunction(weapon1Stats, weapon1Name, weapon1NamePL);
 arrayWeaponFunction(weapon2Stats, weapon2Name, weapon2NamePL);
 arrayWeaponFunction(weapon3Stats, weapon3Name, weapon3NamePL);
 arrayWeaponFunction(weapon4Stats, weapon4Name, weapon4NamePL);
 arrayWeaponFunction(weapon5Stats, weapon5Name, weapon5NamePL);

 function arrayWeaponFunction(stats, statName, statNamePL){

    var statH4 = document.createElement('h4');
    var stat0 = document.createElement('input');
    var stat1 = document.createElement('input');
    var stat2 = document.createElement('input');
    var stat3 = document.createElement('input');
    var stat4 = document.createElement('input');
    var stat5 = document.createElement('input');

    stat2.type="number"
    stat3.type="number"
    stat4.type="number"

    stats[0] = statName + '0';
    stats[1] = statName + '1';
    stats[2] = statName + '2';
    stats[3] = statName + '3';
    stats[4] = statName + '4';
    stats[5] = statName + '5';

    stat0.id = stats[0];
    stat1.id = stats[1];
    stat2.id = stats[2];
    stat3.id = stats[3];
    stat4.id = stats[4];
    stat5.id = stats[5];

    stat0.className = "form-control";
    stat1.className = "form-control";
    stat2.className = "form-control";
    stat3.className = "form-control";
    stat4.className = "form-control";
    stat5.className = "form-control";

    statH4.textContent = statNamePL;

    weapons.appendChild(statH4);
    weapons.appendChild(stat0);
    weapons.appendChild(stat1);
    weapons.appendChild(stat2);
    weapons.appendChild(stat3);
    weapons.appendChild(stat4);
    weapons.appendChild(stat5);
}

//armor
const armors = document.querySelector('#armors');

 var armor1Stats = [];
 var armor1Name = 'armor1';
 var armor1NamePL = 'Armor nr. 1'
 var armor2Stats = [];
 var armor2Name = 'armor2';
 var armor2NamePL = 'Armor nr. 2'
 var armor3Stats = [];
 var armor3Name = 'armor3';
 var armor3NamePL = 'Armor nr. 3'
 var armor4Stats = [];
 var armor4Name = 'armor4';
 var armor4NamePL = 'Armor nr. 4'
 var armor5Stats = [];
 var armor5Name = 'armor5';
 var armor5NamePL = 'Armor nr. 5'

 arrayArmorFunction(armor1Stats, armor1Name, armor1NamePL);
 arrayArmorFunction(armor2Stats, armor2Name, armor2NamePL);
 arrayArmorFunction(armor3Stats, armor3Name, armor3NamePL);
 arrayArmorFunction(armor4Stats, armor4Name, armor4NamePL);
 arrayArmorFunction(armor5Stats, armor5Name, armor5NamePL);

 function arrayArmorFunction(stats, statName, statNamePL){

    var statH4 = document.createElement('h4');
    var stat0 = document.createElement('input');
    var stat1 = document.createElement('input');

    stat1.type="number"

    stats[0] = statName + '0';
    stats[1] = statName + '1';

    stat0.id = stats[0];
    stat1.id = stats[1];

    stat0.className = "form-control";
    stat1.className = "form-control";

    statH4.textContent = statNamePL;

    armors.appendChild(statH4);
    armors.appendChild(stat0);
    armors.appendChild(stat1);
}

var backButton = document.getElementById('backButton');

backButton.addEventListener('click', function(){
  document.location.href = 'cardList.html';
});

//Dodaj karte
postCard.addEventListener('click', function () {

  user = firebase.auth().currentUser.email;

  var game_master = document.getElementById('game_master').value;
  var campaign = document.getElementById('campaign').value;
  var name = document.getElementById('name').value;
  var race = document.getElementById('race').value;
  var profession = document.getElementById('profession').value;
  var previous_profession = document.getElementById('previous_profession').value;
  var gender = document.getElementById('gender').value;
  var age = document.getElementById('age').value;
  var height = document.getElementById('height').value;
  var weight = document.getElementById('weight').value;
  var eyes = document.getElementById('eyes').value;
  var hair = document.getElementById('hair').value;
  var star_sign = document.getElementById('star_sign').value;
  var marks = document.getElementById('marks').value;
  var birthplace = document.getElementById('birthplace').value;
  var family = document.getElementById('family').value;
  var social_status = document.getElementById('social_status').value;
  var past = document.getElementById('past').value;
  var motivation = document.getElementById('motivation').value;
  var serves = document.getElementById('serves').value;
  var friends = document.getElementById('friends').value;
  var foes = document.getElementById('foes').value;
  var likes = document.getElementById('likes').value;
  var dislikes = document.getElementById('dislikes').value;
  var personality = document.getElementById('personality').value;
  var goals = document.getElementById('goals').value;

  var wealthAllItems = [];

  tabIdItemsWealth.forEach(oneItemsId => {
    wealthAllItems.push(document.getElementById(oneItemsId).value);
  });

  var gold = document.getElementById('gold').value;
  var silver = document.getElementById('silver').value;
  var bronze = document.getElementById('bronze').value;

  function skillsAddStatus(skillName){
    var skill = [];
    for(i=0;i<3;i++){
      skill[i] = document.getElementById(skillName + i).checked;
    }
    return skill;
  }

  var command = skillsAddStatus('command');
  var gamble = skillsAddStatus('gamble');
  var ride = skillsAddStatus('ride');
  var drinking = skillsAddStatus('drinking');
  var animals = skillsAddStatus('animals');
  var gossip = skillsAddStatus('gossip');
  var swim = skillsAddStatus('swim');
  var drive = skillsAddStatus('drive');
  var charm = skillsAddStatus('charm');
  var search = skillsAddStatus('search');
  var creep = skillsAddStatus('creep');
  var perception = skillsAddStatus('perception');
  var survival = skillsAddStatus('survival');
  var haggle = skillsAddStatus('haggle');
  var hiding = skillsAddStatus('hiding');
  var rowing = skillsAddStatus('rowing');
  var climbing = skillsAddStatus('climbing');
  var evaluate = skillsAddStatus('evaluate');
  var intimidation = skillsAddStatus('intimidation');

  function bolliny(parametr) {
    var bool0 = false
    var bool1 = false;
    var bool2 = false;
    if (parametr[2]) {
      bool0 = true;
      bool1 = true;
      bool2 = true;
    } else if (parametr[1]) {
      bool0 = true;
      bool1 = true;
    } else if (parametr[0]) {
      bool0 = true;
    }

    return arrryList = [bool0, bool1, bool2]
  };

  var knowlegeAllItems = [];

  tabIdItemsKnowlege.forEach(oneItemsId => {
    knowlegeAllItems.push(document.getElementById(oneItemsId).value);
  });

  var languagesAllItems = [];

  tabIdItemsLanguages.forEach(oneItemsId => {
    languagesAllItems.push(document.getElementById(oneItemsId).value);
  });

  var abilitiesAllItems = [];

  tabIdItemsAbilities.forEach(oneItemsId => {
    abilitiesAllItems.push(document.getElementById(oneItemsId).value);
  });

  var meleeArray = [];

  meleeStats.forEach(oneItemsId => {
    meleeArray.push(document.getElementById(oneItemsId).value);
  });

  var rangeArray = [];

  rangeStats.forEach(oneItemsId => {
    rangeArray.push(document.getElementById(oneItemsId).value);
  });

  var strengthArray = [];

  strengthStats.forEach(oneItemsId => {
    strengthArray.push(document.getElementById(oneItemsId).value);
  });

  var toughnessArray = [];

  toughnessStats.forEach(oneItemsId => {
    toughnessArray.push(document.getElementById(oneItemsId).value);
  });

  var agilityArray = [];

  agilityStats.forEach(oneItemsId => {
    agilityArray.push(document.getElementById(oneItemsId).value);
  });

  var intelligenceArray = [];

  intelligenceStats.forEach(oneItemsId => {
    intelligenceArray.push(document.getElementById(oneItemsId).value);
  });

  var will_powerArray = [];

  will_powerStats.forEach(oneItemsId => {
    will_powerArray.push(document.getElementById(oneItemsId).value);
  });

  var fellowshipArray = [];

  fellowshipStats.forEach(oneItemsId => {
    fellowshipArray.push(document.getElementById(oneItemsId).value);
  });

  var attacksArray = [];

  attacksStats.forEach(oneItemsId => {
    attacksArray.push(document.getElementById(oneItemsId).value);
  });

  var woundsArray = [];

  woundsStats.forEach(oneItemsId => {
    woundsArray.push(document.getElementById(oneItemsId).value);
  });

  function Round(n, k)
  {
      var factor = Math.pow(10, k);
      return Math.round(n*factor)/factor;
  }

  var movementArray = [];

  movementStats.forEach(oneItemsId => {
    movementArray.push(document.getElementById(oneItemsId).value);
  });

  var magicArray = [];

  magicStats.forEach(oneItemsId => {
    magicArray.push(document.getElementById(oneItemsId).value);
  });

  var instanityArray = [];

  instanityStats.forEach(oneItemsId => {
    instanityArray.push(document.getElementById(oneItemsId).value);
  });

  var fortuneArray = [];

  fortuneStats.forEach(oneItemsId => {
    fortuneArray.push(document.getElementById(oneItemsId).value);
  });

  var damageAllItems = [];

  tabIdItemsDamage.forEach(oneItemsId => {
    damageAllItems.push(document.getElementById(oneItemsId).value);
  });

  var weapon1Array = [];

  weapon1Stats.forEach(oneItemsId => {
    weapon1Array.push(document.getElementById(oneItemsId).value);
  });

  var weapon2Array = [];

  weapon2Stats.forEach(oneItemsId => {
    weapon2Array.push(document.getElementById(oneItemsId).value);
  });

  var weapon3Array = [];

  weapon3Stats.forEach(oneItemsId => {
    weapon3Array.push(document.getElementById(oneItemsId).value);
  });

  var weapon4Array = [];

  weapon4Stats.forEach(oneItemsId => {
    weapon4Array.push(document.getElementById(oneItemsId).value);
  });

  var weapon5Array = [];

  weapon5Stats.forEach(oneItemsId => {
    weapon5Array.push(document.getElementById(oneItemsId).value);
  });

  var armor1Array = [];

  armor1Stats.forEach(oneItemsId => {
    armor1Array.push(document.getElementById(oneItemsId).value);
  });

  var armor2Array = [];

  armor2Stats.forEach(oneItemsId => {
    armor2Array.push(document.getElementById(oneItemsId).value);
  });

  var armor3Array = [];

  armor3Stats.forEach(oneItemsId => {
    armor3Array.push(document.getElementById(oneItemsId).value);
  });

  var armor4Array = [];

  armor4Stats.forEach(oneItemsId => {
    armor4Array.push(document.getElementById(oneItemsId).value);
  });

  var armor5Array = [];

  armor5Stats.forEach(oneItemsId => {
    armor5Array.push(document.getElementById(oneItemsId).value);
  });

  var data = {
    game_master: game_master,
    campaign: campaign,
    name: name,
    race: race,
    profession: profession,
    previous_profession: previous_profession,
    gender: gender,
    age: age,
    height: height,
    weight: weight,
    eyes: eyes,
    hair: hair,
    star_sign: star_sign,
    marks: marks,

    birthplace: birthplace,
    family: family,
    social_status: social_status,
    past: past,
    motivation: motivation,
    serves: serves,
    friends: friends,
    foes: foes,
    likes: likes,
    dislikes: dislikes,
    personality: personality,
    goals: goals,

    wealth: wealthAllItems,

    gold: gold,
    silver: silver,
    bronze: bronze,

    command: [bolliny(command)[0], bolliny(command)[1], bolliny(command)[2], 'Ogl'],
    gamble: [bolliny(gamble)[0], bolliny(gamble)[1], bolliny(gamble)[2], 'Int'],
    ride: [bolliny(ride)[0], bolliny(ride)[1], bolliny(ride)[2], 'Zr'],
    drinking: [bolliny(drinking)[0], bolliny(drinking)[1], bolliny(drinking)[2], 'Odp'],
    animals: [bolliny(animals)[0], bolliny(animals)[1], bolliny(animals)[2], 'Int'],
    gossip: [bolliny(gossip)[0], bolliny(gossip)[1], bolliny(gossip)[2], 'Ogd'],
    swim: [bolliny(swim)[0], bolliny(swim)[1], bolliny(swim)[2], 'K'],
    drive: [bolliny(drive)[0], bolliny(drive)[1], bolliny(drive)[2], 'K'],
    charm: [bolliny(charm)[0], bolliny(charm)[1], bolliny(charm)[2], 'Ogd'],
    search: [bolliny(search)[0], bolliny(search)[1], bolliny(search)[2], 'Int'],
    creep: [bolliny(creep)[0], bolliny(creep)[1], bolliny(creep)[2], 'Zr'],
    perception: [bolliny(perception)[0], bolliny(perception)[1], bolliny(perception)[2], 'Int'],
    survival: [bolliny(survival)[0], bolliny(survival)[1], bolliny(survival)[2], 'Int'],
    haggle: [bolliny(haggle)[0], bolliny(haggle)[1], bolliny(haggle)[2], 'Ogd'],
    hiding: [bolliny(hiding)[0], bolliny(hiding)[1], bolliny(hiding)[2], 'Zr'],
    rowing: [bolliny(rowing)[0], bolliny(rowing)[1], bolliny(rowing)[2], 'K'],
    climbing: [bolliny(climbing)[0], bolliny(climbing)[1], bolliny(climbing)[2], 'K'],
    evaluate: [bolliny(evaluate)[0], bolliny(evaluate)[1], bolliny(evaluate)[2], 'Int'],
    intimidation: [bolliny(intimidation)[0], bolliny(intimidation)[1], bolliny(intimidation)[2], 'K'],

    knowledge: knowlegeAllItems,
    languages: languagesAllItems,
    abilities: abilitiesAllItems,

    melee: meleeArray,
    range: rangeArray,
    strength: strengthArray,
    toughness: toughnessArray,
    agility: agilityArray,
    intelligence: intelligenceArray,
    will_power: will_powerArray,
    fellowship: fellowshipArray,

    attacks: attacksArray,
    wounds: woundsArray,
    strength_bonus: ['' + Round(strengthArray[0]/10, 0),'' + Round(strengthArray[1]/10, 0),'' + Round(strengthArray[2]/10, 0)],
    toughness_bonus: ['' + Round(toughnessArray[0]/10, 0),'' + Round(toughnessArray[1]/10, 0),'' + Round(toughnessArray[2]/10, 0)],
    movement: movementArray,
    magic: magicArray,
    insanity: instanityArray,
    fortune: fortuneArray,

    damage: damageAllItems,

    weapon1: weapon1Array,
    weapon2: weapon2Array,
    weapon3: weapon3Array,
    weapon4: weapon4Array,
    weapon5: weapon5Array,

    armor1: armor1Array,
    armor2: armor2Array,
    armor3: armor3Array,
    armor4: armor4Array,
    armor5: armor5Array,
  };

  // Add a new document in collection
  var setDoc = db.collection(user).doc().set(data);

  Swal.fire({
    position: 'top-end',
    type: 'success',
    title: 'Twoja karta została zapisana!',
    showConfirmButton: false,
    timer: 1500
  })

  setTimeout(function () {
    document.location.href = 'cardList.html';
  }, 1500)

});