require('firebase/auth')

var firebase = require("firebase/app");

var firebaseConfig = {
  apiKey: "AIzaSyB5rekUAmGSlShTH1DWFCNaFAL_LRqYzmY",
  authDomain: "warhammer-kp.firebaseapp.com",
  databaseURL: "https://warhammer-kp.firebaseio.com",
  projectId: "warhammer-kp",
  storageBucket: "warhammer-kp.appspot.com",
  messagingSenderId: "773354415332",
  appId: "1:773354415332:web:4bd7470f1532af70"
};

firebase.initializeApp(firebaseConfig);

var admin = require("firebase-admin");

var serviceAccount = require("./warhammer-kp-firebase-adminsdk-sm594-d504c58b61.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://warhammer-kp.firebaseio.com"
});

const db = admin.firestore();

var addCard = document.getElementById('addCard');

const cardList = document.querySelector('#card-list');

var user;

//Dodaj karte
addCard.addEventListener('click', function () {

  document.location.href = './addCard.html';

});

const rollButton = document.querySelector('#rollButton');

var roll = document.getElementById('roll');

roll.addEventListener('click', function (){

  while (rollButton.firstChild) {
    rollButton.removeChild(rollButton.firstChild);
  };

  let resoultLabel = document.createElement('h4')
  let rollResult = document.createElement('span');
  resoultLabel.textContent = "Twoj wynik rzutu dwoma kostkami dziesięciościennymi to:";
  rollResult.textContent = (Math.floor(Math.random() * 10) + 1)*(Math.floor(Math.random() * 10) + 1);
  rollButton.appendChild(resoultLabel);
  rollButton.appendChild(rollResult);
});

refresh();

function refresh() {
  setTimeout(function () {

    while (cardList.firstChild) {
      cardList.removeChild(cardList.firstChild);
    };

    user = firebase.auth().currentUser.email;

    db.collection(user).get().then((snapshot) => {
      snapshot.docs.forEach(doc => {
        renderCard(doc);
      })
    })
  }, 1500)
};

function renderCard(doc) {

  let li = document.createElement('li')
  let game_master = document.createElement('span')
  let campaign = document.createElement('span')
  let name = document.createElement('span')

  let oneCard = document.createElement('button')
  let deleteCard = document.createElement('button')

  li.className = "list-group-item"
  name.className = "showName"
  oneCard.className = "btn btn-primary"
  deleteCard.className = "btn btn-secondary"

  deleteCard.id = 'crudButtons';

  li.setAttribute('data-id', doc.id)

  oneCard.setAttribute('data-id', doc.id)
  oneCard.textContent = 'Pokaż';

  deleteCard.setAttribute('data-id', doc.id)
  deleteCard.textContent = 'Usuń';

  name.textContent = doc.data().name;
  game_master.textContent = doc.data().game_master;
  campaign.textContent = doc.data().campaign;

  li.appendChild(oneCard);
  li.appendChild(deleteCard);
  li.appendChild(name);
  li.appendChild(game_master);
  li.appendChild(campaign);

  cardList.appendChild(li);

  oneCard.onclick = function () {
    var dist = oneCard.getAttribute('data-id');
    if (dist != "") {
      document.location.href = 'pageOneCard.html?dist=' + dist;
    } else
      alert('Oops.!!')
  };

  deleteCard.onclick = function () {

    Swal.fire({
      title: 'Jesteś pewny?',
      text: "To zostanie stracone na zawsze!",
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Nie, usuwaj!',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Tak, usuń to!'
    }).then((result) => {
      if (result.value) {
        var dist = oneCard.getAttribute('data-id');
        var deleteDoc = db.collection(user).doc(dist).delete();
        refresh();
        Swal.fire(
          'Usunięte!',
          'Ta karta została pomyślnie usunięta.',
          'success'
        )
      }
    })
  }

};


























 //dodanie tablicy array

    // var command = document.getElementById('command').value;

    // if (command >= 20){
    //   var plus20 = true;
    //   var plus10 = false;
    //   var haveSome = false;
    // }
    // else if((command >= 10)&&(command < 20)){
    //   var plus20 = false;
    //   var plus10 = true;
    //   var haveSome = false;
    // }
    // else if((command >=1)&&(command < 10)){
    //   var plus20 = false;
    //   var plus10 = false;
    //   var haveSome = true;
    // }
    // else{
    //   var plus20 = false;
    //   var plus10 = false;
    //   var haveSome = false;
    // };

    // var data = {
    //   name: name,
    //   Command: [plus20, plus10, haveSome, command]
    // };