// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

require('firebase/auth')

var firebase = require("firebase/app");

var firebaseConfig = {
    apiKey: "AIzaSyB5rekUAmGSlShTH1DWFCNaFAL_LRqYzmY",
    authDomain: "warhammer-kp.firebaseapp.com",
    databaseURL: "https://warhammer-kp.firebaseio.com",
    projectId: "warhammer-kp",
    storageBucket: "warhammer-kp.appspot.com",
    messagingSenderId: "773354415332",
    appId: "1:773354415332:web:4bd7470f1532af70"
  };

firebase.initializeApp(firebaseConfig);

var signUpBtn = document.getElementById('signUpBtn');
var signInBtn = document.getElementById('signInBtn');

signUpBtn.addEventListener('click', function(){
    var emailField = document.getElementById('email').value;
    var passwordField = document.getElementById('password').value;

    firebase.auth().createUserWithEmailAndPassword(emailField, passwordField).then(function(){
        Swal.fire({
            title: 'Konto zostało utowrzone!',
            imageUrl: './images/gif3.gif',
            imageWidth: 300,
            imageHeight: 150,
          })
    }).catch(function(error){
        if(error != null){
            console.log(error.message);
            Swal.fire({
                title: 'Coś poszło nie tak!',
                imageUrl: './images/fota2.png',
                imageWidth: 200,
                imageHeight: 200,
              })
            return;
        }
    });
});

signInBtn.addEventListener('click', function(){
    var emailField = document.getElementById('email').value;
    var passwordField = document.getElementById('password').value;

    firebase.auth().signInWithEmailAndPassword(emailField, passwordField).then(function(){
        document.location.href = 'cardList.html';
    }).catch(function(error){
        Swal.fire({
            //title: 'Nie ma takiego użytownika lub hasła!',
            title: '<h3 style="color: black;">Nie ma takiego użytownika lub hasła!</h3>',
            //title: '',
            //background: 'center left #fff url(./images/fota2e.png)',
            //background: 'center left url(./images/fota2e.png)',
            imageUrl: './images/fota3.jpg',
            imageWidth: 300,
            imageHeight: 150,
            imageAlt: 'Custom image',
            animation: false
          })
    });
});



