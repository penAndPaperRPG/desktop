require('firebase/auth')

var firebase = require("firebase/app");

var firebaseConfig = {
  apiKey: "AIzaSyB5rekUAmGSlShTH1DWFCNaFAL_LRqYzmY",
  authDomain: "warhammer-kp.firebaseapp.com",
  databaseURL: "https://warhammer-kp.firebaseio.com",
  projectId: "warhammer-kp",
  storageBucket: "warhammer-kp.appspot.com",
  messagingSenderId: "773354415332",
  appId: "1:773354415332:web:4bd7470f1532af70"
};

firebase.initializeApp(firebaseConfig);

var admin = require("firebase-admin");

var serviceAccount = require("./warhammer-kp-firebase-adminsdk-sm594-d504c58b61.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://warhammer-kp.firebaseio.com"
});

const db = admin.firestore();

var urlId = document.URL.indexOf('?dist=');
var urlId2 = document.URL.substring(urlId, document.URL.length).replace('?dist=', '');

var user;

refresh();

function refresh() {
  setTimeout(function () {

    while (cardList.firstChild) {
      cardList.removeChild(cardList.firstChild);
    };

    while (cardListSkill.firstChild) {
      cardListSkill.removeChild(cardListSkill.firstChild);
    };

    while (cardListItem.firstChild) {
      cardListItem.removeChild(cardListItem.firstChild);
    };

    while (cardListStats.firstChild) {
      cardListStats.removeChild(cardListStats.firstChild);
    };

    while (cardListDamage.firstChild) {
      cardListDamage.removeChild(cardListDamage.firstChild);
    };

    while (cardListEq.firstChild) {
      cardListEq.removeChild(cardListEq.firstChild);
    };

    user = firebase.auth().currentUser.email;

    var cards = db.collection(user).doc(urlId2);

    var getDoc = cards.get().then(doc => {
      if (!doc.exists) {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Nie ma takiej karty!',
          showConfirmButton: true
        })
      } else {
        renderCard(doc);
        renderCardItems(doc);
        renderCardSkills(doc);
        renderCardStats(doc);
        renderCardDamage(doc);
        renderCardEq(doc);
      }
    })
      .catch(err => {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Coś poszło nie tak!',
        })
      });

  }, 1500)
};

//dane
const cardList = document.querySelector('#card-list');

function renderCard(doc) {

  var oneElemntPl = 0;

  let elementsAllPl = [
    'Mistrz gry',
    'Kompania',

    'Nazwa',
    'Rasa',
    'Profesja',
    'Poprzednia profesja',
    'Płeć',
    'Wiek',
    'Wysokość',
    'Waga',
    'Kolor oczu',
    'Włosy',
    'Znak gwiezdny',
    'Znaki szczególne',

    'Pochodzenie',
    'Rodzina',
    'Pozycja społeczna',
    'Kim wcześniej był',
    'Dlaczego wyruszył na szlak',
    'Komu służy',
    'Przyjaciele',
    'Wrogowie',
    'Co lubi',
    'Czego nie lubi',
    'Osobowość i zachowania',
    'Cele',
  ]

  let elementsAll = [
    'game_master',
    'campaign',

    'name',
    'race',
    'profession',
    'previous_profession',
    'gender',
    'age',
    'height',
    'weight',
    'eyes',
    'hair',
    'star_sign',
    'marks',

    'birthplace',
    'family',
    'social_status',
    'past',
    'motivation',
    'serves',
    'friends',
    'foes',
    'likes',
    'dislikes',
    'personality',
    'goals',
  ]

  elementsAll.forEach(oneElementAll => {
    let oneElementLi = document.createElement('li')
    let oneElementData = document.createElement('span')
    let oneElement = document.createElement('span')

    oneElementLi.className = "list-group-item";

    oneElement.contentEditable = true;

    oneElement.textContent = doc.get(oneElementAll);
    oneElement.id = oneElementAll;
    oneElementData.textContent = elementsAllPl[oneElemntPl] + ':';
    oneElemntPl++;

    oneElementLi.appendChild(oneElementData);
    oneElementLi.appendChild(oneElement);

    cardList.appendChild(oneElementLi);
  });

  let liButtons = document.createElement('div')
  let cofnijOneCard = document.createElement('button')
  let editOneCard = document.createElement('button')

  cofnijOneCard.id = "cofnijOneCard";
  cofnijOneCard.className = "btn btn-primary";
  editOneCard.id = "editOneCard";
  editOneCard.className = "btn btn-info";

  cofnijOneCard.textContent = 'Wróć'
  editOneCard.textContent = 'Zaktualizuj dane'

  liButtons.appendChild(cofnijOneCard);
  liButtons.appendChild(editOneCard);

  cardList.appendChild(liButtons);

  cofnijOneCard.onclick = function () {
    document.location.href = "cardList.html";
  };

  editOneCard.onclick = function () {

    var game_master = document.getElementById('game_master').innerHTML;
    var campaign = document.getElementById('campaign').innerHTML
    var name = document.getElementById('name').innerHTML;
    var race = document.getElementById('race').innerHTML;
    var profession = document.getElementById('profession').innerHTML;
    var previous_profession = document.getElementById('previous_profession').innerHTML;
    var gender = document.getElementById('gender').innerHTML;
    var age = document.getElementById('age').innerHTML;
    var height = document.getElementById('height').innerHTML;
    var weight = document.getElementById('weight').innerHTML;
    var eyes = document.getElementById('eyes').innerHTML;
    var hair = document.getElementById('hair').innerHTML;
    var star_sign = document.getElementById('star_sign').innerHTML;
    var marks = document.getElementById('marks').innerHTML;

    var birthplace = document.getElementById('birthplace').innerHTML;
    var family = document.getElementById('family').innerHTML;
    var social_status = document.getElementById('social_status').innerHTML;
    var past = document.getElementById('past').innerHTML;
    var motivation = document.getElementById('motivation').innerHTML;
    var serves = document.getElementById('serves').innerHTML;
    var friends = document.getElementById('friends').innerHTML;
    var foes = document.getElementById('foes').innerHTML;
    var likes = document.getElementById('likes').innerHTML;
    var dislikes = document.getElementById('dislikes').innerHTML;
    var personality = document.getElementById('personality').innerHTML;
    var goals = document.getElementById('goals').innerHTML;

    var data = {

      game_master: game_master,
      campaign: campaign,

      name: name,
      race: race,
      profession: profession,
      previous_profession: previous_profession,
      gender: gender,
      age: age,
      height: height,
      weight: weight,
      eyes: eyes,
      hair: hair,
      star_sign: star_sign,
      marks: marks,

      birthplace: birthplace,
      family: family,
      social_status: social_status,
      past: past,
      motivation: motivation,
      serves: serves,
      friends: friends,
      foes: foes,
      likes: likes,
      dislikes: dislikes,
      personality: personality,
      goals: goals,
    };

    var setDoc = db.collection(user).doc(urlId2).update(data);

    refresh();

    setTimeout(function () {
      Swal.fire({
        //position: 'top-end',
        type: 'success',
        title: 'Twoja karta została zaktualizowana!',
        showConfirmButton: false,
        timer: 1500
      })
    }, 1500)
  };


};
//Wyposarzenie
const cardListItem = document.querySelector('#card-list-items');

function renderCardItems(doc) {

  let wealth = 'wealth';
  let money = ['gold', 'silver', 'bronze']
  let moneyPL = ['Złoto', 'Srebro', 'Bronz']
  let moneyPlCalc = 0;
  let oneElementLi = document.createElement('li')
  let oneElementData = document.createElement('span')
  let arrayItemsId = [];
  let itemNumber = 0;

  //elementy ekwipunku
  oneElementLi.appendChild(oneElementData);

  doc.get(wealth).forEach(oneWealthItem => {
    let oneElement = document.createElement('span')
    let separateDiv = document.createElement('div')
    oneElement.textContent = oneWealthItem;
    oneElement.contentEditable = true;
    oneElement.id = 'wealth' + itemNumber;
    itemNumber++;
    arrayItemsId.push(oneElement.id);
    oneElementLi.appendChild(separateDiv);
    separateDiv.appendChild(oneElement);
  });

  oneElementLi.className = "list-group-item";
  oneElementData.textContent = 'Przedmioty:';
  cardListItem.appendChild(oneElementLi);

  //przyciski add i delete
  let addDeleteItems = document.createElement('div')
  let buttonAddItem = document.createElement('button')
  let buttonDeleteItems = document.createElement('button')

  buttonAddItem.id = "addWealth";
  buttonAddItem.className = "btn btn-primary";
  buttonDeleteItems.id = "deleteWealth";
  buttonDeleteItems.className = "btn btn-info";

  buttonAddItem.textContent = 'Dodaj'
  buttonDeleteItems.textContent = 'Usuń'

  buttonAddItem.onclick = function () {
    let oneElement = document.createElement('span')
    let separateDiv = document.createElement('div')
    oneElement.textContent = 'nowy';
    oneElement.contentEditable = true;
    oneElement.id = 'wealth' + itemNumber;
    itemNumber++;
    arrayItemsId.push(oneElement.id);
    separateDiv.appendChild(oneElement);
    oneElementLi.appendChild(separateDiv);
  };

  buttonDeleteItems.onclick = function () {
    Swal.fire({
      title: 'Jesteś pewny?',
      text: "To zostanie stracone na zawsze!",
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Nie, usuwaj!',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Tak, usuń to!'
    }).then((result) => {
      if (result.value) {

        arrayItemsId = [];

        var data = {
          wealth: arrayItemsId,
        }

        var setDoc = db.collection(user).doc(urlId2).update(data);

        refresh();

        Swal.fire(
          'Usunięte!',
          'Twoje przedmioty zostały usunięte.',
          'success'
        )
      }
    })
  };

  addDeleteItems.appendChild(buttonAddItem);
  addDeleteItems.appendChild(buttonDeleteItems);
  cardListItem.appendChild(addDeleteItems);

  //kasa
  money.forEach(oneMoneyElement => {
    let oneElement = document.createElement('span')
    let oneElementLi = document.createElement('li')
    let oneElementData = document.createElement('span')
    oneElementLi.className = "list-group-item";
    oneElementData.textContent = moneyPL[moneyPlCalc] + ':';
    moneyPlCalc++;
    oneElement.textContent = doc.get(oneMoneyElement);
    oneElement.contentEditable = true;
    oneElement.id = oneMoneyElement;
    oneElementLi.appendChild(oneElementData);
    oneElementLi.appendChild(oneElement);
    cardListItem.appendChild(oneElementLi);
  });

  //przyciski
  let liButtons = document.createElement('div')
  let cofnijOneCard = document.createElement('button')
  let editOneCard = document.createElement('button')

  cofnijOneCard.id = "cofnijOneCard";
  cofnijOneCard.className = "btn btn-primary";
  editOneCard.id = "editOneCard";
  editOneCard.className = "btn btn-info";

  cofnijOneCard.textContent = 'Wróć'
  editOneCard.textContent = 'Zaktualizuj wyposarzenie'

  liButtons.appendChild(cofnijOneCard);
  liButtons.appendChild(editOneCard);

  cardListItem.appendChild(liButtons);

  cofnijOneCard.onclick = function () {
    document.location.href = "cardList.html";
  };

  editOneCard.onclick = function () {

    var idArrayItems = [];
    arrayItemsId.forEach(oneArrayItemsId => {
      idArrayItems.push(document.getElementById(oneArrayItemsId).innerHTML)
    });

    var gold = document.getElementById('gold').innerHTML;
    var silver = document.getElementById('silver').innerHTML
    var bronze = document.getElementById('bronze').innerHTML;

    var data = {
      wealth: idArrayItems,
      gold: gold,
      silver: silver,
      bronze: bronze,
    };

    var setDoc = db.collection(user).doc(urlId2).update(data);

    refresh();

    setTimeout(function () {
      Swal.fire({
        type: 'success',
        title: 'Twoja karta została zaktualizowana!',
        showConfirmButton: false,
        timer: 1500
      })
    }, 1500)
  };

};

//skile
const cardListSkill = document.querySelector('#card-list-skill');

function renderCardSkills(doc) {

  var elementsAllPlNumber = 0;
  var elementsAllPl = [
    'Dowodzenie',
    'Hazard',
    'Jeździectwo',
    'Mocna głowa',
    'Opieka nad zwierzętami',
    'Plotkowanie',
    'Pływanie',
    'Powożenie',
    'Przekonywanie',
    'Przeszukiwanie',
    'Skradanie się',
    'Spostrzegawczość',
    'Sztuka przetrwania',
    'Targowanie',
    'Ukrywanie się',
    'Wioślarstwo',
    'Wspinaczka',
    'Wycena',
    'Zastraszanie',
  ];

  let elementsAll = [
    'command',
    'gamble',
    'ride',
    'drinking',
    'animals',
    'gossip',
    'swim',
    'drive',
    'charm',
    'search',
    'creep',
    'perception',
    'survival',
    'haggle',
    'hiding',
    'rowing',
    'climbing',
    'evaluate',
    'intimidation',
  ];

  let arrayKnowledge = [];
  let arrayIdKnowledge = [];
  let numberKnowledge = 1;
  let knowledge = 'Wiedza';
  let addKnowledge = 'addKnowledge';
  let deleteKnowledge = 'deleteKnowledge';

  arrayForCustomSkills(arrayKnowledge, arrayIdKnowledge, numberKnowledge, 'knowledge', knowledge, addKnowledge, deleteKnowledge);

  let arrayLanguages = [];
  let arrayIdLanguages = [];
  let numberLanguages = 1;
  let languages = 'Język';
  let addLanguages = 'addLanguages';
  let deleteLanguages = 'deleteLanguages';

  arrayForCustomSkills(arrayLanguages, arrayIdLanguages, numberLanguages, 'languages', languages, addLanguages, deleteLanguages);

  let arrayAbilities = [];
  let arrayIdAbilities = [];
  let numberAbilities = 1;
  let abilities = 'Zdolności';
  let addAbilities = 'addAbilities';
  let deleteAbilities = 'deleteAbilities';

  arrayForCustomSkills(arrayAbilities, arrayIdAbilities, numberAbilities, 'abilities', abilities, addAbilities, deleteAbilities);

  function arrayForCustomSkills (arraySkills, arrayIdSkill, numberSkill, stringName, variable, addSkill, deleteSkill){

    let oneElementLi= document.createElement('li')
    let oneElementData = document.createElement('span')
    oneElementLi.appendChild(oneElementData);

    doc.get(stringName).forEach(eachItems =>{
      arraySkills.push(eachItems);
    });

    arraySkills.forEach(oneItemsInArrayItems =>{
      let oneElement = document.createElement('span')
      let separateDiv = document.createElement('div')
      oneElement.textContent = oneItemsInArrayItems;
      oneElement.contentEditable = true;
      oneElement.id = stringName + numberSkill;
      numberSkill++;
      arrayIdSkill.push(oneElement.id);
      oneElementLi.appendChild(separateDiv);
      separateDiv.appendChild(oneElement);
    });

    oneElementLi.className="list-group-item";
    oneElementData.textContent = variable +':';
    cardListSkill.appendChild(oneElementLi);

    //dalej przyciski
    let addDeleteItems = document.createElement('div')
    let buttonAddItem = document.createElement('button')
    let buttonDeleteItems = document.createElement('button')
  
    buttonAddItem.id = addSkill;
    buttonAddItem.className = "btn btn-primary";
    buttonDeleteItems.id = deleteSkill;
    buttonDeleteItems.className = "btn btn-info";
  
    buttonAddItem.textContent = 'Dodaj'
    buttonDeleteItems.textContent = 'Usuń'
  
    buttonAddItem.onclick = function () {
      let oneElement = document.createElement('span')
      let separateDiv = document.createElement('div')
      oneElement.textContent = 'nowy';
      oneElement.contentEditable = true;
      oneElement.id = stringName + numberSkill;
      numberSkill++;
      arrayIdSkill.push(oneElement.id);
      separateDiv.appendChild(oneElement);
      oneElementLi.appendChild(separateDiv);
    };
  
    buttonDeleteItems.onclick = function () {
      Swal.fire({
        title: 'Jesteś pewny?',
        text: "To zostanie stracone na zawsze!",
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Nie, usuwaj!',
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Tak, usuń to!'
      }).then((result) => {
        if (result.value) {

          arraySkills = [];

          if(stringName==='knowledge'){
            var data = {
              knowledge: arraySkills
            }
          }else if(stringName==='languages'){
            var data = {
              languages: arraySkills
            }
          }else if(stringName==='abilities'){
            var data = {
              abilities: arraySkills
            }
          }
  
          var setDoc = db.collection(user).doc(urlId2).update(data);
  
          refresh();
  
          Swal.fire(
            'Usunięte!',
            'Twoje przedmioty zostały usunięte.',
            'success'
          )
        }
      })
    };
  
    addDeleteItems.appendChild(buttonAddItem);
    addDeleteItems.appendChild(buttonDeleteItems);
    cardListSkill.appendChild(addDeleteItems);
    //koniec przyciskow
  }
  
  elementsAll.forEach(oneElementAll => {
    let oneElementLi = document.createElement('li')
    let oneElementData = document.createElement('span')
    let oneElement = document.createElement('span')
    let oneElement0 = document.createElement('input')
    let oneElement0Span = document.createElement('Span')
    let oneElement1 = document.createElement('input')
    let oneElement1Span = document.createElement('Span')
    let oneElement2 = document.createElement('input')
    let oneElement2Span = document.createElement('Span')
    let separateDiv = document.createElement('div');

    oneElement0.type = "checkbox";
    oneElement1.type = "checkbox";
    oneElement2.type = "checkbox";

    oneElement0.className = "labelCSS";
    oneElement1.className = "labelCSS";
    oneElement2.className = "labelCSS";

    oneElementLi.className = "list-group-item";

    oneElement0Span.textContent = "Posiada: "
    oneElement1Span.textContent = "+10: "
    oneElement2Span.textContent = "+20: "

    oneElement.textContent = '(' + doc.get(oneElementAll)[3] + '):';

    oneElement.id = oneElementAll;
    oneElementData.textContent = elementsAllPl[elementsAllPlNumber];
    elementsAllPlNumber++;

    oneElement0.id = oneElementAll + '0'
    oneElement1.id = oneElementAll + '1'
    oneElement2.id = oneElementAll + '2'

    var oneElementCondition0 = doc.get(oneElementAll)[0];
    var oneElementCondition1 = doc.get(oneElementAll)[1];
    var oneElementCondition2 = doc.get(oneElementAll)[2];

    if (oneElementCondition2) {
      oneElement0.checked = true;
      oneElement1.checked = true;
      oneElement2.checked = true;
    } else if (oneElementCondition1) {
      oneElement0.checked = true;
      oneElement1.checked = true;
    } else if (oneElementCondition0) {
      oneElement0.checked = true;
    }

    oneElement0.onclick = function () {
      if (oneElement0.checked) {
      } else if (oneElement0.checked == false) {
        oneElement0 = true;
        oneElement1.checked = false;
        oneElement2.checked = false;
      }
    };
    oneElement1.onclick = function () {
      if (oneElement1.checked) {
        oneElement0.checked = true;
      } else if (oneElement1.checked == false) {
        oneElement0.checked = false;
        oneElement2.checked = false;
      }
    };
    oneElement2.onclick = function () {
      if (oneElement2.checked) {
        oneElement0.checked = true;
        oneElement1.checked = true;
      } else if (oneElement2.checked == false) {
        oneElement0.checked = false;
        oneElement1.checked = false;
      }
    };

    oneElementLi.appendChild(oneElementData);
    oneElementLi.appendChild(oneElement);
    oneElementLi.appendChild(separateDiv);
    separateDiv.appendChild(oneElement0Span);
    separateDiv.appendChild(oneElement0);
    separateDiv.appendChild(oneElement1Span);
    separateDiv.appendChild(oneElement1);
    separateDiv.appendChild(oneElement2Span);
    separateDiv.appendChild(oneElement2);
    cardListSkill.appendChild(oneElementLi);
  });

  let liButtons = document.createElement('div')
  let cofnijOneCard = document.createElement('button')
  let editOneCard = document.createElement('button')

  cofnijOneCard.id = "cofnijOneCard";
  cofnijOneCard.className = "btn btn-primary";
  editOneCard.id = "editOneCard";
  editOneCard.className = "btn btn-info";

  cofnijOneCard.textContent = 'Wróć'
  editOneCard.textContent = 'Zaktualizuj umiejętności'

  liButtons.appendChild(cofnijOneCard);
  liButtons.appendChild(editOneCard);

  cardListSkill.appendChild(liButtons);

  cofnijOneCard.onclick = function () {
    document.location.href = "cardList.html";
  };

  editOneCard.onclick = function () {

    function skillsAddStatus(skillName) {
      var skill = [];
      for (i = 0; i < 3; i++) {
        skill[i] = document.getElementById(skillName + i).checked;
      }

      return skill;
    }

    var command = skillsAddStatus('command');
    var gamble = skillsAddStatus('gamble');
    var ride = skillsAddStatus('ride');
    var drinking = skillsAddStatus('drinking');
    var animals = skillsAddStatus('animals');
    var gossip = skillsAddStatus('gossip');
    var swim = skillsAddStatus('swim');
    var drive = skillsAddStatus('drive');
    var charm = skillsAddStatus('charm');
    var search = skillsAddStatus('search');
    var creep = skillsAddStatus('creep');
    var perception = skillsAddStatus('perception');
    var survival = skillsAddStatus('survival');
    var haggle = skillsAddStatus('haggle');
    var hiding = skillsAddStatus('hiding');
    var rowing = skillsAddStatus('rowing');
    var climbing = skillsAddStatus('climbing');
    var evaluate = skillsAddStatus('evaluate');
    var intimidation = skillsAddStatus('intimidation');

    var arrayKnowledgeData = [];
    arrayIdKnowledge.forEach(oneArrayItemsId => {
      arrayKnowledgeData.push(document.getElementById(oneArrayItemsId).innerHTML)
    });

    var arrayLanguageData = [];
    arrayIdLanguages.forEach(oneArrayItemsId => {
      arrayLanguageData.push(document.getElementById(oneArrayItemsId).innerHTML)
    });

    var arrayAbilitiesData = [];
    arrayIdAbilities.forEach(oneArrayItemsId => {
      arrayAbilitiesData.push(document.getElementById(oneArrayItemsId).innerHTML)
    });


    var data = {
      command: [bolliny(command)[0], bolliny(command)[1], bolliny(command)[2], 'Ogl'],
      gamble: [bolliny(gamble)[0], bolliny(gamble)[1], bolliny(gamble)[2], 'Int'],
      ride: [bolliny(ride)[0], bolliny(ride)[1], bolliny(ride)[2], 'Zr'],
      drinking: [bolliny(drinking)[0], bolliny(drinking)[1], bolliny(drinking)[2], 'Odp'],
      animals: [bolliny(animals)[0], bolliny(animals)[1], bolliny(animals)[2], 'Int'],
      gossip: [bolliny(gossip)[0], bolliny(gossip)[1], bolliny(gossip)[2], 'Ogd'],
      swim: [bolliny(swim)[0], bolliny(swim)[1], bolliny(swim)[2], 'K'],
      drive: [bolliny(drive)[0], bolliny(drive)[1], bolliny(drive)[2], 'K'],
      charm: [bolliny(charm)[0], bolliny(charm)[1], bolliny(charm)[2], 'Ogd'],
      search: [bolliny(search)[0], bolliny(search)[1], bolliny(search)[2], 'Int'],
      creep: [bolliny(creep)[0], bolliny(creep)[1], bolliny(creep)[2], 'Zr'],
      perception: [bolliny(perception)[0], bolliny(perception)[1], bolliny(perception)[2], 'Int'],
      survival: [bolliny(survival)[0], bolliny(survival)[1], bolliny(survival)[2], 'Int'],
      haggle: [bolliny(haggle)[0], bolliny(haggle)[1], bolliny(haggle)[2], 'Ogd'],
      hiding: [bolliny(hiding)[0], bolliny(hiding)[1], bolliny(hiding)[2], 'Zr'],
      rowing: [bolliny(rowing)[0], bolliny(rowing)[1], bolliny(rowing)[2], 'K'],
      climbing: [bolliny(climbing)[0], bolliny(climbing)[1], bolliny(climbing)[2], 'K'],
      evaluate: [bolliny(evaluate)[0], bolliny(evaluate)[1], bolliny(evaluate)[2], 'Int'],
      intimidation: [bolliny(intimidation)[0], bolliny(intimidation)[1], bolliny(intimidation)[2], 'K'],

      knowledge: arrayKnowledgeData,
      languages: arrayLanguageData,
      abilities: arrayAbilitiesData,
    };

    function bolliny(parametr) {
      var bool0 = false
      var bool1 = false;
      var bool2 = false;
      if (parametr[2]) {
        bool0 = true;
        bool1 = true;
        bool2 = true;
      } else if (parametr[1]) {
        bool0 = true;
        bool1 = true;
      } else if (parametr[0]) {
        bool0 = true;
      }

      return arrryList = [bool0, bool1, bool2]
    };

    var setDoc = db.collection(user).doc(urlId2).update(data);

    refresh();

    setTimeout(function () {
      Swal.fire({
        //position: 'top-end',
        type: 'success',
        title: 'Twoja karta została zaktualizowana!',
        showConfirmButton: false,
        timer: 1500
      })
    }, 1500)
  };
};

//staty
const cardListStats = document.querySelector('#card-list-stats');

function renderCardStats(doc) {

var meleeFB = [];
 var meleeStats = [];
 var meleeName = 'melee';
 var meleeNamePL = 'WW'
 var rangeFB = [];
 var rangeStats = [];
 var rangeName = 'range';
 var rangeNamePL = 'US'
 var strengthFB = [];
 var strengthStats = [];
 var strengthName = 'strength';
 var strengthNamePL = 'K'
 var toughnessFB = [];
 var toughnessStats = [];
 var toughnessName = 'toughness';
 var toughnessNamePL = 'Odp'
 var agilityFB = [];
 var agilityStats = [];
 var agilityName = 'agility';
 var agilityNamePL = 'Zr'
 var intelligence = [];
 var intelligenceStats = [];
 var intelligenceName = 'intelligence';
 var intelligenceNamePL = 'Int'
 var will_power = [];
 var will_powerStats = [];
 var will_powerName = 'will_power';
 var will_powerNamePL = 'Sw'
 var fellowshipFB = [];
 var fellowshipStats = [];
 var fellowshipName = 'fellowship';
 var fellowshipNamePL = 'Ogd';

 var attacksFB = [];
 var attacksStats = [];
 var attacksName = 'attacks';
 var attacksNamePL = 'A'
 var woundsFB = [];
 var woundsStats = [];
 var woundsName = 'wounds';
 var woundsNamePL = 'Żyw'
 var strength_bonusFB = [];
 var strength_bonusStats = [];
 var strength_bonusName = 'strength_bonus';
 var strength_bonusNamePL = 'S'
 var toughness_bonusFB = [];
 var toughness_bonusStats = [];
 var toughness_bonusName = 'toughness_bonus';
 var toughness_bonusNamePL = 'Wt'
  var movementFB = [];
  var movementStats = [];
  var movementName = 'movement';
  var movementNamePL = 'Sz'
  var magicFB = [];
  var magicStats = [];
  var magicName = 'magic';
  var magicNamePL = 'Mag'
  var instanityFB = [];
  var instanityStats = [];
  var instanityName = 'insanity';
  var instanityNamePL = 'PO'
  var fortuneFB = [];
  var fortuneStats = [];
  var fortuneName = 'fortune';
  var fortuneNamePL = 'PP'

 arrayStatsFunction(meleeStats, meleeName, meleeNamePL, meleeFB);
 arrayStatsFunction(rangeStats, rangeName, rangeNamePL, rangeFB);
 arrayStatsFunction(strengthStats, strengthName, strengthNamePL, strengthFB);
 arrayStatsFunction(toughnessStats, toughnessName, toughnessNamePL, toughnessFB);
 arrayStatsFunction(agilityStats, agilityName, agilityNamePL, agilityFB);
 arrayStatsFunction(intelligenceStats, intelligenceName, intelligenceNamePL, intelligence);
 arrayStatsFunction(will_powerStats, will_powerName, will_powerNamePL, will_power);
 arrayStatsFunction(fellowshipStats, fellowshipName, fellowshipNamePL, fellowshipFB);
 arrayStatsFunction(attacksStats, attacksName, attacksNamePL, attacksFB);
 arrayStatsFunction(woundsStats, woundsName, woundsNamePL, woundsFB);

 arrayStatsFunction(strength_bonusStats, strength_bonusName, strength_bonusNamePL, strength_bonusFB);
 arrayStatsFunction(toughness_bonusStats, toughness_bonusName, toughness_bonusNamePL, toughness_bonusFB);

 arrayStatsFunction(movementStats, movementName, movementNamePL, movementFB);
 arrayStatsFunction(magicStats, magicName, magicNamePL, magicFB);
 arrayStatsFunction(instanityStats, instanityName, instanityNamePL, instanityFB);
 arrayStatsFunction(fortuneStats, fortuneName, fortuneNamePL, fortuneFB);

 document.getElementById('strength_bonus0').contentEditable = false;
 document.getElementById('strength_bonus1').contentEditable = false;
 document.getElementById('strength_bonus2').contentEditable = false;
 document.getElementById('toughness_bonus0').contentEditable = false;
 document.getElementById('toughness_bonus1').contentEditable = false;
 document.getElementById('toughness_bonus2').contentEditable = false;

 function arrayStatsFunction(stats, statName, statNamePL, statFB){

    doc.get(statName).forEach(eachItems =>{
      statFB.push(eachItems);
    });

    var statli = document.createElement('li');
    var statLabel = document.createElement('span');
    var stat0 = document.createElement('span');
    var stat1 = document.createElement('span');
    var stat2 = document.createElement('span');

    stat0.type="number"
    stat1.type="number"
    stat2.type="number"

    statLabel.textContent = statNamePL + ':';
    stat0.textContent = statFB[0];
    stat1.textContent = statFB[1];
    stat2.textContent = statFB[2];

    stats[0] = statName + '0';
    stats[1] = statName + '1';
    stats[2] = statName + '2';

    stat0.id = stats[0];
    stat1.id = stats[1];
    stat2.id = stats[2];

    stat0.contentEditable = true;
    stat1.contentEditable = true;
    stat2.contentEditable = true;

    statli.className = "list-group-item";

    statli.appendChild(statLabel);
    statli.appendChild(stat0);
    statli.appendChild(stat1);
    statli.appendChild(stat2);
    cardListStats.appendChild(statli);
}
  let liButtons = document.createElement('div')
  let cofnijOneCard = document.createElement('button')
  let editOneCard = document.createElement('button')

  cofnijOneCard.id = "cofnijOneCard";
  cofnijOneCard.className = "btn btn-primary";
  editOneCard.id = "editOneCard";
  editOneCard.className = "btn btn-info";

  cofnijOneCard.textContent = 'Wróć'
  editOneCard.textContent = 'Zaktualizuj statystyki'

  liButtons.appendChild(cofnijOneCard);
  liButtons.appendChild(editOneCard);

  cardListStats.appendChild(liButtons);

  cofnijOneCard.onclick = function () {
    document.location.href = "cardList.html";
  };

  editOneCard.onclick = function () {

    var meleeArray = [];

    meleeStats.forEach(oneItemsId => {
      meleeArray.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var rangeArray = [];
  
    rangeStats.forEach(oneItemsId => {
      rangeArray.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var strengthArray = [];
  
    strengthStats.forEach(oneItemsId => {
      strengthArray.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var toughnessArray = [];
  
    toughnessStats.forEach(oneItemsId => {
      toughnessArray.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var agilityArray = [];
  
    agilityStats.forEach(oneItemsId => {
      agilityArray.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var intelligenceArray = [];
  
    intelligenceStats.forEach(oneItemsId => {
      intelligenceArray.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var will_powerArray = [];
  
    will_powerStats.forEach(oneItemsId => {
      will_powerArray.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var fellowshipArray = [];
  
    fellowshipStats.forEach(oneItemsId => {
      fellowshipArray.push(document.getElementById(oneItemsId).innerHTML);
    });

    var attacksArray = [];

    attacksStats.forEach(oneItemsId => {
      attacksArray.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var woundsArray = [];
  
    woundsStats.forEach(oneItemsId => {
      woundsArray.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    function Round(n, k)
    {
        var factor = Math.pow(10, k);
        return Math.round(n*factor)/factor;
    }
  
    var movementArray = [];
  
    movementStats.forEach(oneItemsId => {
      movementArray.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var magicArray = [];
  
    magicStats.forEach(oneItemsId => {
      magicArray.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var instanityArray = [];
  
    instanityStats.forEach(oneItemsId => {
      instanityArray.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var fortuneArray = [];
  
    fortuneStats.forEach(oneItemsId => {
      fortuneArray.push(document.getElementById(oneItemsId).innerHTML);
    });


    var data = {
      melee: meleeArray,
      range: rangeArray,
      strength: strengthArray,
      toughness: toughnessArray,
      agility: agilityArray,
      intelligence: intelligenceArray,
      will_power: will_powerArray,
      fellowship: fellowshipArray,

      attacks: attacksArray,
      wounds: woundsArray,
      strength_bonus: ['' + Round(strengthArray[0]/10, 0), '' + Round(strengthArray[1]/10, 0), '' + Round(strengthArray[2]/10, 0)],
      toughness_bonus: ['' + Round(toughnessArray[0]/10, 0), '' + Round(toughnessArray[1]/10, 0), '' + Round(toughnessArray[2]/10, 0)],
      movement: movementArray,
      magic: magicArray,
      insanity: instanityArray,
      fortune: fortuneArray,
    };

    var setDoc = db.collection(user).doc(urlId2).update(data);

    refresh();

    setTimeout(function () {
      Swal.fire({
        //position: 'top-end',
        type: 'success',
        title: 'Twoja karta została zaktualizowana!',
        showConfirmButton: false,
        timer: 1500
      })
    }, 1500)
  };
};

//Rany
const cardListDamage= document.querySelector('#card-list-damage');

function renderCardDamage(doc) {

  let damage = 'damage';
  let oneElementLi = document.createElement('li')
  let oneElementData = document.createElement('span')
  let arrayItemsId = [];
  let itemNumber = 0;

  //elementy ekwipunku
  oneElementLi.appendChild(oneElementData);

  doc.get(damage).forEach(oneDamageItem => {
    let oneElement = document.createElement('span')
    let separateDiv = document.createElement('div')
    oneElement.textContent = oneDamageItem;
    oneElement.contentEditable = true;
    oneElement.id = 'damage' + itemNumber;
    itemNumber++;
    arrayItemsId.push(oneElement.id);
    oneElementLi.appendChild(separateDiv);
    separateDiv.appendChild(oneElement);
  });

  oneElementLi.className = "list-group-item";
  oneElementData.textContent = 'Rany:';
  cardListDamage.appendChild(oneElementLi);

  //przyciski add i delete
  let addDeleteItems = document.createElement('div')
  let buttonAddItem = document.createElement('button')
  let buttonDeleteItems = document.createElement('button')

  buttonAddItem.id = "addDamage";
  buttonAddItem.className = "btn btn-primary";
  buttonDeleteItems.id = "deleteDamage";
  buttonDeleteItems.className = "btn btn-info";

  buttonAddItem.textContent = 'Dodaj'
  buttonDeleteItems.textContent = 'Usuń'

  buttonAddItem.onclick = function () {
    let oneElement = document.createElement('span')
    let separateDiv = document.createElement('div')
    oneElement.textContent = 'nowy';
    oneElement.contentEditable = true;
    oneElement.id = 'damage' + itemNumber;
    itemNumber++;
    arrayItemsId.push(oneElement.id);
    separateDiv.appendChild(oneElement);
    oneElementLi.appendChild(separateDiv);
  };

  buttonDeleteItems.onclick = function () {
    Swal.fire({
      title: 'Jesteś pewny?',
      text: "To zostanie stracone na zawsze!",
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: 'Nie, usuwaj!',
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Tak, usuń to!'
    }).then((result) => {
      if (result.value) {

        arrayItemsId = [];

        var data = {
          damage: arrayItemsId,
        }

        var setDoc = db.collection(user).doc(urlId2).update(data);

        refresh();

        Swal.fire(
          'Usunięte!',
          'Twoje przedmioty zostały usunięte.',
          'success'
        )
      }
    })
  };

  addDeleteItems.appendChild(buttonAddItem);
  addDeleteItems.appendChild(buttonDeleteItems);
  cardListDamage.appendChild(addDeleteItems);

  //przyciski
  let liButtons = document.createElement('div')
  let cofnijOneCard = document.createElement('button')
  let editOneCard = document.createElement('button')

  cofnijOneCard.id = "cofnijOneCard";
  cofnijOneCard.className = "btn btn-primary";
  editOneCard.id = "editOneCard";
  editOneCard.className = "btn btn-info";

  cofnijOneCard.textContent = 'Wróć'
  editOneCard.textContent = 'Zaktualizuj rany'

  liButtons.appendChild(cofnijOneCard);
  liButtons.appendChild(editOneCard);

  cardListDamage.appendChild(liButtons);

  cofnijOneCard.onclick = function () {
    document.location.href = "cardList.html";
  };

  editOneCard.onclick = function () {

    var idArrayItems = [];
    arrayItemsId.forEach(oneArrayItemsId => {
      idArrayItems.push(document.getElementById(oneArrayItemsId).innerHTML)
    });

    var data = {
      damage: idArrayItems,
    };

    var setDoc = db.collection(user).doc(urlId2).update(data);

    refresh();

    setTimeout(function () {
      Swal.fire({
        type: 'success',
        title: 'Twoja karta została zaktualizowana!',
        showConfirmButton: false,
        timer: 1500
      })
    }, 1500)
  };

};

//Sprzęt
const cardListEq = document.querySelector('#card-list-eq');

function renderCardEq(doc) {

 var weapon1FB = []
 var weapon1Stats = [];
 var weapon1Name = 'weapon1';
 var weapon1NamePL = 'Weapon nr. 1'
 var weapon2FB = []
 var weapon2Stats = [];
 var weapon2Name = 'weapon2';
 var weapon2NamePL = 'Weapon nr. 2'
 var weapon3FB = []
 var weapon3Stats = [];
 var weapon3Name = 'weapon3';
 var weapon3NamePL = 'Weapon nr. 3'
 var weapon4FB = []
 var weapon4Stats = [];
 var weapon4Name = 'weapon4';
 var weapon4NamePL = 'Weapon nr. 4'
 var weapon5FB = []
 var weapon5Stats = [];
 var weapon5Name = 'weapon5';
 var weapon5NamePL = 'Weapon nr. 5'

 arrayWeaponFunction(weapon1Stats, weapon1Name, weapon1NamePL, weapon1FB);
 arrayWeaponFunction(weapon2Stats, weapon2Name, weapon2NamePL, weapon2FB);
 arrayWeaponFunction(weapon3Stats, weapon3Name, weapon3NamePL, weapon3FB);
 arrayWeaponFunction(weapon4Stats, weapon4Name, weapon4NamePL, weapon4FB);
 arrayWeaponFunction(weapon5Stats, weapon5Name, weapon5NamePL, weapon5FB);

 function arrayWeaponFunction(stats, statName, statNamePL, statFB){

    doc.get(statName).forEach(eachItems =>{
      statFB.push(eachItems);
    });

    var statli = document.createElement('li');
    var statLabel = document.createElement('span');
    var stat0 = document.createElement('span');
    var stat1 = document.createElement('span');
    var stat2 = document.createElement('span');
    var stat3 = document.createElement('span');
    var stat4 = document.createElement('span');
    var stat5 = document.createElement('span');

    stat2.type="number"
    stat3.type="number"
    stat4.type="number"

    statLabel.textContent = statNamePL + ':';
    stat0.textContent = statFB[0];
    stat1.textContent = statFB[1];
    stat2.textContent = statFB[2];
    stat3.textContent = statFB[3];
    stat4.textContent = statFB[4];
    stat5.textContent = statFB[5];

    stats[0] = statName + '0';
    stats[1] = statName + '1';
    stats[2] = statName + '2';
    stats[3] = statName + '3';
    stats[4] = statName + '4';
    stats[5] = statName + '5';

    stat0.id = stats[0];
    stat1.id = stats[1];
    stat2.id = stats[2];
    stat3.id = stats[3];
    stat4.id = stats[4];
    stat5.id = stats[5];

    stat0.contentEditable = true;
    stat1.contentEditable = true;
    stat2.contentEditable = true;
    stat3.contentEditable = true;
    stat4.contentEditable = true;
    stat5.contentEditable = true;

    statli.className = "list-group-item";

    statli.appendChild(statLabel);
    statli.appendChild(stat0);
    statli.appendChild(stat1);
    statli.appendChild(stat2);
    statli.appendChild(stat3);
    statli.appendChild(stat4);
    statli.appendChild(stat5);
    cardListEq.appendChild(statli);
}


//armors

var armor1FB = []
var armor1Stats = [];
var armor1Name = 'armor1';
var armor1NamePL = 'Armor nr. 1'
var armor2FB = []
var armor2Stats = [];
var armor2Name = 'armor2';
var armor2NamePL = 'Armor nr. 2'
var armor3FB = []
var armor3Stats = [];
var armor3Name = 'armor3';
var armor3NamePL = 'Armor nr. 3'
var armor4FB = []
var armor4Stats = [];
var armor4Name = 'armor4';
var armor4NamePL = 'Armor nr. 4'
var armor5FB = []
var armor5Stats = [];
var armor5Name = 'armor5';
var armor5NamePL = 'Armor nr. 5'

arrayArmorFunction(armor1FB, armor1Stats, armor1Name, armor1NamePL);
arrayArmorFunction(armor2FB, armor2Stats, armor2Name, armor2NamePL);
arrayArmorFunction(armor3FB, armor3Stats, armor3Name, armor3NamePL);
arrayArmorFunction(armor4FB, armor4Stats, armor4Name, armor4NamePL);
arrayArmorFunction(armor5FB, armor5Stats, armor5Name, armor5NamePL);

function arrayArmorFunction(statFB, stats, statName, statNamePL){

   doc.get(statName).forEach(eachItems =>{
     statFB.push(eachItems);
   });

   var statli = document.createElement('li');
   var statLabel = document.createElement('span');
   var stat0 = document.createElement('span');
   var stat1 = document.createElement('span');

   stat1.type="number"

   statLabel.textContent = statNamePL + ':';
   stat0.textContent = statFB[0];
   stat1.textContent = statFB[1];

   stats[0] = statName + '0';
   stats[1] = statName + '1';

   stat0.id = stats[0];
   stat1.id = stats[1];

   stat0.contentEditable = true;
   stat1.contentEditable = true;

   statli.className = "list-group-item";

   statli.appendChild(statLabel);
   statli.appendChild(stat0);
   statli.appendChild(stat1);
   cardListEq.appendChild(statli);
}

  let liButtons = document.createElement('div')
  let cofnijOneCard = document.createElement('button')
  let editOneCard = document.createElement('button')

  cofnijOneCard.id = "cofnijOneCard";
  cofnijOneCard.className = "btn btn-primary";
  editOneCard.id = "editOneCard";
  editOneCard.className = "btn btn-info";

  cofnijOneCard.textContent = 'Wróć'
  editOneCard.textContent = 'Zaktualizuj ekwipunek'

  liButtons.appendChild(cofnijOneCard);
  liButtons.appendChild(editOneCard);

  cardListEq.appendChild(liButtons);

  cofnijOneCard.onclick = function () {
    document.location.href = "cardList.html";
  };

  editOneCard.onclick = function () {

    var weapon1Array = [];

    weapon1Stats.forEach(oneItemsId => {
      weapon1Array.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var weapon2Array = [];
  
    weapon2Stats.forEach(oneItemsId => {
      weapon2Array.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var weapon3Array = [];
  
    weapon3Stats.forEach(oneItemsId => {
      weapon3Array.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var weapon4Array = [];
  
    weapon4Stats.forEach(oneItemsId => {
      weapon4Array.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var weapon5Array = [];
  
    weapon5Stats.forEach(oneItemsId => {
      weapon5Array.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var armor1Array = [];
  
    armor1Stats.forEach(oneItemsId => {
      armor1Array.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var armor2Array = [];
  
    armor2Stats.forEach(oneItemsId => {
      armor2Array.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var armor3Array = [];
  
    armor3Stats.forEach(oneItemsId => {
      armor3Array.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var armor4Array = [];
  
    armor4Stats.forEach(oneItemsId => {
      armor4Array.push(document.getElementById(oneItemsId).innerHTML);
    });
  
    var armor5Array = [];
  
    armor5Stats.forEach(oneItemsId => {
      armor5Array.push(document.getElementById(oneItemsId).innerHTML);
    });

    var data = {
      weapon1: weapon1Array,
      weapon2: weapon2Array,
      weapon3: weapon3Array,
      weapon4: weapon4Array,
      weapon5: weapon5Array,
  
      armor1: armor1Array,
      armor2: armor2Array,
      armor3: armor3Array,
      armor4: armor4Array,
      armor5: armor5Array,
    };

    var setDoc = db.collection(user).doc(urlId2).update(data);

    refresh();

    setTimeout(function () {
      Swal.fire({
        //position: 'top-end',
        type: 'success',
        title: 'Twoja karta została zaktualizowana!',
        showConfirmButton: false,
        timer: 1500
      })
    }, 1500)
  };
};



