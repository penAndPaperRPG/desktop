var command0 = document.getElementById('command0');
var command1 = document.getElementById('command1');
var command2 = document.getElementById('command2');
var command = [command0, command1, command2];
var gamble0 = document.getElementById('gamble0');
var gamble1 = document.getElementById('gamble1');
var gamble2 = document.getElementById('gamble2');
var gamble = [gamble0, gamble1, gamble2];
var ride0 = document.getElementById('ride0');
var ride1 = document.getElementById('ride1');
var ride2 = document.getElementById('ride2');
var ride = [ride0, ride1, ride2];
var drinking0 = document.getElementById('drinking0');
var drinking1 = document.getElementById('drinking1');
var drinking2 = document.getElementById('drinking2');
var drinking = [drinking0, drinking1, drinking2];
var animals0 = document.getElementById('animals0');
var animals1 = document.getElementById('animals1');
var animals2 = document.getElementById('animals2');
var animals = [animals0, animals1, animals2];
var gossip0 = document.getElementById('gossip0');
var gossip1 = document.getElementById('gossip1');
var gossip2 = document.getElementById('gossip2');
var gossip = [gossip0, gossip1, gossip2];
var swim0 = document.getElementById('swim0');
var swim1 = document.getElementById('swim1');
var swim2 = document.getElementById('swim2');
var swim = [swim0, swim1, swim2];
var drive0 = document.getElementById('drive0');
var drive1 = document.getElementById('drive1');
var drive2 = document.getElementById('drive2');
var drive = [drive0, drive1, drive2];
var charm0 = document.getElementById('charm0');
var charm1 = document.getElementById('charm1');
var charm2 = document.getElementById('charm2');
var charm = [charm0, charm1, charm2];
var search0 = document.getElementById('search0');
var search1 = document.getElementById('search1');
var search2 = document.getElementById('search2');
var search = [search0, search1, search2];
var creep0 = document.getElementById('creep0');
var creep1 = document.getElementById('creep1');
var creep2 = document.getElementById('creep2');
var creep = [creep0, creep1, creep2];
var perception0 = document.getElementById('perception0');
var perception1 = document.getElementById('perception1');
var perception2 = document.getElementById('perception2');
var perception = [perception0, perception1, perception2];
var survival0 = document.getElementById('survival0');
var survival1 = document.getElementById('survival1');
var survival2 = document.getElementById('survival2');
var survival = [survival0, survival1, survival2];
var haggle0 = document.getElementById('haggle0');
var haggle1 = document.getElementById('haggle1');
var haggle2 = document.getElementById('haggle2');
var haggle = [haggle0, haggle1, haggle2];
var hiding0 = document.getElementById('hiding0');
var hiding1 = document.getElementById('hiding1');
var hiding2 = document.getElementById('hiding2');
var hiding = [hiding0, hiding1, hiding2];
var rowing0 = document.getElementById('rowing0');
var rowing1 = document.getElementById('rowing1');
var rowing2 = document.getElementById('rowing2');
var rowing = [rowing0, rowing1, rowing2];
var climbing0 = document.getElementById('climbing0');
var climbing1 = document.getElementById('climbing1');
var climbing2 = document.getElementById('climbing2');
var climbing = [climbing0, climbing1, climbing2];
var evaluate0 = document.getElementById('evaluate0');
var evaluate1 = document.getElementById('evaluate1');
var evaluate2 = document.getElementById('evaluate2');
var evaluate = [evaluate0, evaluate1, evaluate2];
var intimidation0 = document.getElementById('intimidation0');
var intimidation1 = document.getElementById('intimidation1');
var intimidation2 = document.getElementById('intimidation2');
var intimidation = [intimidation0, intimidation1, intimidation2];

clickCheckbox(command);
clickCheckbox(gamble);
clickCheckbox(ride);
clickCheckbox(drinking);
clickCheckbox(animals);
clickCheckbox(gossip);
clickCheckbox(swim);
clickCheckbox(drive);
clickCheckbox(charm);
clickCheckbox(search);
clickCheckbox(creep);
clickCheckbox(perception);
clickCheckbox(survival);
clickCheckbox(haggle);
clickCheckbox(hiding);
clickCheckbox(rowing);
clickCheckbox(climbing);
clickCheckbox(evaluate);
clickCheckbox(intimidation);

function clickCheckbox (parametr){
    var checkbox0 = parametr[0];
    var checkbox1 = parametr[1];
    var checkbox2 = parametr[2];

    checkbox0.onclick = function() {
        if(checkbox0.checked){
        }else if(checkbox0.checked==false){
            checkbox1.checked = false;
            checkbox2.checked = false;
        }
    };
    
    checkbox1.onclick = function() {
        if(checkbox1.checked){
            checkbox0.checked = true;
        }else if(checkbox1.checked==false){
            checkbox0.checked = false;
            checkbox2.checked = false;
        }
    };
    
    checkbox2.onclick = function() {
        if(checkbox2.checked){
            checkbox0.checked = true;
            checkbox1.checked = true;
        }else if(checkbox2.checked==false){
            checkbox0.checked = false;
            checkbox1.checked = false;
        }
    };
};